# Flectra Community / connector-interfaces

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[connector_importer_source_sftp](connector_importer_source_sftp/) | 2.0.1.1.0| Add import source capable of loading files from SFTP.
[connector_importer](connector_importer/) | 2.0.2.2.1| This module takes care of import sessions.
[connector_importer_demo](connector_importer_demo/) | 2.0.1.0.0| Demo module for Connector Importer.
[connector_importer_product](connector_importer_product/) | 2.0.2.1.0| Ease definition of product imports using `connector_importer`.


