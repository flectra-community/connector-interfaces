# Copyright 2019 Camptocamp SA
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl)

from flectra.addons.component.core import Component


class FlectraRecordHandlerCSVStd(Component):
    """Interact w/ flectra importable records from standard Flectra CSV files."""

    _name = "importer.flectrarecord.handler.csv.std"
    _inherit = "importer.flectrarecord.handler"
    _usage = "flectrarecord.handler.csv"
    xmlid_key = "id"  # CSV field containing the record XML-ID

    def flectra_find(self, values, orig_values, use_xmlid=False):
        """Find any existing item in flectra based on the XML-ID."""
        if use_xmlid:
            if not self.xmlid_key:
                return self.model
            item = self.env.ref(values[self.xmlid_key], raise_if_not_found=False)
            return item
        return super().flectra_find(values, orig_values)

    def flectra_exists(self, values, orig_values, use_xmlid=False):
        """Return true if the items exists."""
        return bool(self.flectra_find(values, orig_values, use_xmlid))

    def flectra_create(self, values, orig_values):
        """Create a new flectra record."""
        raise NotImplementedError(
            "This method is not used when importing standard CSV files."
        )

    def flectra_write(self, values, orig_values):
        """Create a new flectra record."""
        raise NotImplementedError(
            "This method is not used when importing standard CSV files."
        )
